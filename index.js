require('dotenv').config();
import express from 'express';
const mongoose = require('mongoose');
const {onConnection} = require('./socket.js');
import cors from 'cors';
import bodyParser from 'body-parser';
var cookieParser = require('cookie-parser')
 


mongoose.connect(process.env.MONGO_URL,  { useUnifiedTopology: true, useNewUrlParser: true });
mongoose.set('useCreateIndex', true)

// Config app
const app = express();
const port = process.env.PORT || 8080;
app.set('view engine', 'ejs');
app.set('views', 'views');
app.use(express.static('public'));
app.use(cors({
    origin: 'http://localhost:8080',
    credentials: true,
    exposedHeaders: ["set-cookie"],
  }));

// Config routes
const landingRoutes = require('./routes/landing.route');
const chatRoutes = require('./routes/chat.route');
const userRoutes = require('./routes/user.route');
const roomRoutes = require('./routes/room.route');
const messageRoutes = require('./routes/message.route');

// To get body for Submit form
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser())

app.use('/landing', landingRoutes);
app.use('/chat', chatRoutes);
app.use('/user', userRoutes);
app.use('/room', roomRoutes);
app.use('/message', messageRoutes);
// Index route
app.get('/', (req, res) => {
    res.send('Hello World!')
})
// app.listen(port, () => {
//     console.log(`Example app listening at http://localhost:${port}`)
// })

// // Config Socket
const server = require('http').createServer(app)
const io = require('socket.io')(server);
io.on('connection', (socket) => onConnection(socket, io) );

server.listen(port, () => {
    console.log('Server listening on :' + port);
});
 