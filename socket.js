var Room = require('./models/room.model');
var User = require('./models/user.model');
var ObjectId = require('mongoose').Types.ObjectId; 
import _ from 'lodash';

import {createMessage} from './controllers/message.controller';
const userArr = [];
const onConnection = (socket, io) => {    
    console.log('connected: ', socket.id, socket.handshake.query.user);

    //  Join the rooms for the first time connect;
    let userId = socket.handshake.query.user;
    let userObj = false;
    User.findOne({_id: ObjectId(userId)}).exec((err, user) => {
        userObj = user;
    })
    let roomList = []
    Room.find({users: userId}).exec((err, room) => {
        roomList = room;
        room.map(item => {
            socket.join(item._id);
        });
    })
    socket.on('Client-react-chat', (data) => {        
        createMessage({...data, user: ObjectId(userId)});
        io.to(data.room).emit('Server-react-chat', {room: data.room, content: data.data, user: userObj});
    });
    //  Send all rooms to client for the first connect
    io.sockets.emit('server-send-room-list', socket.adapter.rooms);
    
    socket.on('disconnect', () => onDisconnection(socket));
    socket.on('Client-send-data', (data) => {
        // io.sockets.emit('Server-send-data', data); // send to all
        // socket.emit('Server-send-data', data); // send to person sent
        socket.broadcast.emit('Server-send-data', data); // sent to all except person sent
    });
    socket.on('client-send-userName', (data) => {
        if(userArr.includes(data)) {
            socket.emit('server-send-register-fail', data);
        } else {
            socket.userName = data;
            userArr.push(data);
            socket.emit('server-send-register-success', data);
            io.sockets.emit('server-send-user-list', userArr);
        }
    })
    socket.on('client-send-logout', () => {
        userArr.splice(socket.userName, 1);
        io.sockets.emit('server-send-user-list', userArr);
    })
    socket.on('client-send-message', (data) => {        
        io.sockets.emit('server-send-message', {message: data, userName: socket.userName});
    })   
    
    socket.on('client-send-typing', () => {        
        socket.broadcast.emit('server-send-typing', socket.userName);
    })  
    
    socket.on('client-send-typing-stop', () => {        
        socket.broadcast.emit('server-send-typing-stop');
    })   
    
    socket.on('client-send-create-room', (data) => {    
        if(data!='') {
            socket.join(data);
            socket.adapter.rooms[data].userCreated = true;
            io.sockets.emit('server-send-room-list', socket.adapter.rooms);
            // socket.roomMain = data;
        }        
    })

    socket.on('client-send-join-room', data => {
        if(data!='') {
            socket.join(data);
        }
    })
    
    socket.on('client-submit-chat', data => {
        io.to(data.room).emit('server-send-room-chat',{userId: socket.id, content: data.content});
    })
}

const onDisconnection = (socket) => {
    // console.log('User ' + socket.id + ' Disconnected');
    // console.log(socket.adapter.rooms);
}

module.exports = {
    onConnection
}