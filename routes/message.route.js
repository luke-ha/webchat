const express = require('express');
const router = express.Router();
import checkToken from '../auth/checkToken';

const { body, validationResult } = require('express-validator');
const {getMessageByRoom} = require('../controllers/message.controller');

router.get('/getMessageByRoom', checkToken, getMessageByRoom);

module.exports = router;