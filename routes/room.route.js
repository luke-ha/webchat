const express = require('express');
const router = express.Router();
const {postCreate, getRoomByUser, addUserToRoom} = require('../controllers/room.controller');

router.post('/postCreate', postCreate)
router.get('/getRoomByUser', getRoomByUser)
router.post('/addUserToRoom', addUserToRoom)

module.exports = router;