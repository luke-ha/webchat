const express = require('express');
const router = express.Router();
const {postCreate, index} = require('../controllers/room.controller');

router.get('/', (req, res) => {
    res.render('chat/index');
})
// router.get('/room', (req, res) => {
//     res.render('chat/room');
// })

router.get('/room', index)

router.post('/createRoom', postCreate);

module.exports = router;