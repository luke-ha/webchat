const express = require('express');
const router = express.Router();
const {postSignup, postLogin, signup, getUserListByRoom, getAllUser, verifyToken} = require('../controllers/user.controller');

router.get('/getAllUser', getAllUser)
router.get('/getUserListByRoom', getUserListByRoom)
router.post('/login', postLogin)
router.post('/register', signup)
router.post('/signup', postSignup)
router.get('/verifyToken', verifyToken)

module.exports = router;