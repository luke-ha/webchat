const socket = io('http://localhost:3000');
$(document).ready(() => {
    $('.login-form').show();
    $('.chat-form').hide();

    $('.btn-register').click(() => {
        socket.emit('client-send-userName', $('.txt-user-name').val());
    })

    $('.btn-logout').click(() => {
        socket.emit('client-send-logout');
    })
    
    $('.btn-send-message').click(() => {
        socket.emit('client-send-message', $('.txt-message').val());
    })
    $('.txt-message').focusin(() =>{
        socket.emit('client-send-typing');
    })
    $('.txt-message').focusout(() =>{
        socket.emit('client-send-typing-stop');
    })

    

    socket.on('server-send-register-fail', (data) => {
        alert('please type another name');
    })

    socket.on('server-send-register-success', (data) => {
        $('.login-form').hide();
        $('.chat-form').show();
        $('.current-user').html(data);
    })
    
    socket.on('server-send-user-list', (data) => {
        let newList = '';
        data.map((item) => {
            newList += '<div class="user-online">'+item+'</div>';
        })
        $('.box-content').html(newList);
    })
    
    socket.on('server-send-message', (data) => {
        $('.list-messages').append('<div class="message">' + data.userName+ ': ' +data.message+'</div>');
    })
    
    socket.on('server-send-typing', (data) => {
        $('.typing-notification').html(data + ' is typing ... ');
    })
    
    socket.on('server-send-typing-stop', (data) => {
        $('.typing-notification').html('');
    })
    
})