const socket = io('http://localhost:'+3000);
$(document).ready(function(){

    var mainRoom = '';
    function createNewRoom () {
        socket.emit('client-send-create-room', $('.txt-room').val());
    }

    function clickRoom (node) {
        //  Update Header
        document.getElementsByClassName('right-chat-box__room-name')[0].innerHTML = '<span class="room-name__title">ROOM:</span> <b>'+node.innerHTML+'</b>';    
        //  Join to room
        socket.emit('client-send-join-room', node.innerHTML.trim());
        mainRoom = node.innerHTML.trim();
    }

    function sendChat () {
        if(mainRoom){
            let content = document.getElementsByClassName('txt-chat-content')[0].value;
            socket.emit('client-submit-chat',{room: mainRoom, content:  content});
        } else {
            alert('Please choose room to chat!')
        }
    }

    //  Listen event click to Create a room
    $('.btn-submit').click(function(){
        createNewRoom();
    })
    document.getElementsByClassName('header__txt-room')[0].addEventListener('keydown', (event) => {
        if(event.which == 13 || event.keyCode == 13){
            createNewRoom();
        }
    })
    
    //  Listen event click to Chat
    $('.btn-send').click(() => {
        sendChat();
    })
    document.getElementsByClassName('txt-chat-content')[0].addEventListener('keydown', (event) => {
        if(event.which == 13 || event.keyCode == 13){
            sendChat();
        }
    })
    //  List Room list change
    socket.on('server-send-room-list', (data) => {
        let roomListHtml = '';
        for(room in data) {
            if(data[room].userCreated){
                roomListHtml += '<li class="left-room-list__item">'+room+'</li>';
            }
        }
        document.getElementsByClassName('left-room-list__container')[0].innerHTML = roomListHtml;
        //  Listen event click to each Room
        let roomNodes = document.getElementsByClassName('left-room-list__item');
        if(roomNodes.length != 0){
            for(let i = 0; i < roomNodes.length; i++){                
                roomNodes[i].addEventListener('click', () => {
                    clickRoom(roomNodes[i]);
                })
            }
        }        
    })

    //  Listen chat content
    socket.on('server-send-room-chat', data => {
        console.log('receive chat content: ', data);
    })
})