const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        max: 100
    },
    users: {
        type: [String]
    }
})

const Room = mongoose.model('room', roomSchema );

module.exports = Room;