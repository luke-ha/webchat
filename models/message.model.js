const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const messageSchema = new mongoose.Schema({
    content: {
        type: String,
        trim: true,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId, ref: 'user'
    },
    room: { type: Schema.Types.ObjectId, ref: 'room' }
})

const Message = mongoose.model('message', messageSchema );

module.exports = Message;