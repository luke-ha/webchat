const { boolean } = require('@hapi/joi');
const mongoose = require('mongoose');
const Room = require('./room.model');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        max: 64
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true,
        lowercase: true,
        index: true 
    },
    refreshToken: {
        type: String
    },
    is2FAEnabled: {
        type: Boolean
    }

}, {
    timestamps: true
})

const User = mongoose.model('user', userSchema, 'user');

const findByNameAndPassword = (data) => {
    let result = User.find({email: data.email, password: data.password})
    return result;
}
module.exports = User;
// module.exports = {
//     User: User,
//     findByNameAndPassword: findByNameAndPassword
// };