import User  from '../models/user.model';
import registerValidation from '../auth/validation';
import Room from '../models/room.model';
import bcrypt from 'bcryptjs';
const jwt = require("jsonwebtoken")
import smtpTransport from 'nodemailer-smtp-transport';
import nodemailer from 'nodemailer';

export const postLogin = async (req, res) => {
    const {email, password} = req.body;
    let userRes = await  User.findOne({email});
    if(userRes) {
        const passLogin = await bcrypt.compare(password, userRes.password);
        if(!passLogin) {
            return res.json({error: "Username incorrect."})
        }
        const token = jwt.sign({user: userRes}, process.env.SECRET_TOKEN, {expiresIn: 9999});
        res.cookie('access_token', '123213', {
            httpOnly: true
        })
        res.send({message: "Login success.", data: userRes, token: token});
        console.log(res);
    } else {
        return res.json({error: "Username incorrect."})
    }
}

//  Create user without email account activation
export const signup = async (req, res) => {
    const {error} = registerValidation(req.body);    
    if(error) {
        return res.json({error: "Please recheck input."})
    } else {
        const {name, email, password} = req.body;
        const salt = await bcrypt.genSalt(10);
        const hashPass = await bcrypt.hash(password, salt);
        User.findOne({email}).exec((err, user) => {
            if(user) {
                return res.json({error: "User with this email already exists."})
            }
            let newUser = new User({name, email, password: hashPass});
            newUser.save((err, success) => {
                if(err) {
                    return res.json({error: err})
                }
                res.json({
                    message: "Signup success!"
                })
            })
        })
    }
    
}

export const getUserListByRoom = async (req, res) => {
    const {room} = req.query;
    Room.findById(room).exec((err, room) => {
        let {users} = room;
        User.find({'_id': { $in: users }}, (err, user) => {
            console.log(user);
            res.json({
                status: "ok",
                data: user
            })
        })        
    });    
}

export const getAllUser = async (req, res) => {
    User.find().exec((err, user) => {
        res.json({
            status: "ok",
            data: user
        })
    });    
}

export const verifyToken = (req, res) => {
    const token = req.query.token;
    try {
        const checkToken = jwt.verify(token, process.env.SECRET_TOKEN);
        res.send({
            status: 'ok',
            data: "success"
        })
    } catch (error) {
        console.log(error);
        res.send({
            status: 'fail',
            data: "invalid token"
        })
    }
}

export const postSignup = async (req, res) => {
    const {error} = registerValidation(req.body);    
    if(error) {
        return res.status(400).json({error: "Please recheck input."})
    }

    const {name, email, password} = req.body;
    const token = jwt.sign({user: {name, email, password}}, process.env.SECRET_TOKEN, {expiresIn: 120});
    let transporter = nodemailer.createTransport(smtpTransport({    
        service: 'gmail',
        host: 'smtp.gmail.com', 
        auth: {        
             user: 'kennguyentest@gmail.com',        
             pass: 'test123@@3llAA'    
        }
   }));

   //options
   const mailOptions = {
        from: 'kennguyentest@gmail.com',
        to: 'thang.hv.dev@gmail.com',                   // from req.body.to
        subject: 'Email verification',         //from req.body.subject
        html: 'Message'             //from req.body.message
    };

    //delivery
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);  
        } else {     
            console.log('Email sent: ' + info.response);  
        }   
    });

    return res.status(200).json({
        status: 'ok',
        token
    })
}