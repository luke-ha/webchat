var Room = require('../models/room.model')
var ObjectId = require('mongoose').Types.ObjectId; 
import _ from 'lodash';

export const postCreate = (req, res) => {
    const {name, user} = req.body;
    Room.findOne({name}).exec((err, room) => {
        if(room) {
            return res.json({error: "Room with this name already exists."})
        }
        let newRoom = new Room({name, users: [user]});
        newRoom.save((err, success) => {
            if(err) {
                return res.json({error: err})
            }
            res.json({
                message: "Create success!"
            })
        })
    })
}

export const getRoomByUser = (req, res) => {
    console.log(req.cookies.access_token);
    Room.find({users: req.query.user}).exec((err, room) => {
        res.cookie('room', '123213')
        res.json({
            status: "ok",
            data: room
        })
    })
}

export const addUserToRoom = (req, res) => {
    const {room, users} = req.body;
    console.log(room);
    Room.findOne({_id: new ObjectId(room)}).exec((err, room) => {
        console.log(room);
        const newUsers = room.users.concat(users);
        room.users = (_.uniq(newUsers));
        room.save((err, room) => {
            res.json({
                status: "ok",
                data: room
            })
        })        
    })
}

export const index = async (req, res) => {
    let rooms = await Room.find();
    // console.log(rooms);
    res.render('chat/room', { rooms })
}