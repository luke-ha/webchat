var Message = require('../models/message.model')
var ObjectId = require('mongoose').Types.ObjectId; 
import _ from 'lodash';

export const createMessage = (data) => {
    let newMessage = new Message({content: data.data, room: data.room, user: data.user}).save();
}

export const getMessageByRoom = async (req, res) => {
    let message = await Message.find({room: req.query.room}).populate('room', '_id').populate('user', 'name');
    console.log(message);
    res.json({
        status: "ok",
        data: message
    })
}