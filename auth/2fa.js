import qrcode from 'qrcode';
import otplib from 'otplib';


const {authenticator} = otplib;

const generateUniqueSecret = () => {
    return authenticator.generateSecret();
}

const generateOTPToken = (userName, serviceName, secret) => {
    return authenticator.keyuri(userName, serviceName, secret)
}

const verifyOTPToken = (token, secret) => {
    return authenticator.verify({token, secret});
}

const generateQRCode = async (otpAuth) => {
    try {
        const QRCodeImageUrl = await qrcode.toDataURL(otpAuth)
        return `<img src='${QRCodeImageUrl}' alt='qr-code-img-kd' />`
    } catch (error) {
        console.log('Could not generate QR code', error);
        return;
    }
}

export {
    generateUniqueSecret,
    verifyOTPToken,
    generateOTPToken,
    generateQRCode
}