const jwt = require("jsonwebtoken")

module.exports = function(req, res, next) {
   
   const token = req.header('auth-token');    

   if (!token) {
     return res.send({error: "Please Login"})
   }

   try {
        const checkToken = jwt.verify(token, process.env.SECRET_TOKEN)
        req.user = checkToken
        next()
   } catch(err){
        res.send({error: "Please Login"})
   }
}
