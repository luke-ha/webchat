import Joi from '@hapi/joi';

// Register Validate
const registerValidation = (data) => {
    const schema = Joi.object ({
        name: Joi.string()
                 .min(4)
                 .required(),
        email: Joi.string()
                   .email()
                   .min(6)
                   .required(),
        password: Joi.string()
                   .min(6)
                   .required(),
                          
    })
   return  schema.validate(data)
}
export default registerValidation;